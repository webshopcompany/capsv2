﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caps.Services
{
    public class TransactionSrv : ITransactions
    {
        private RestAPICalls _srv;

        public TransactionSrv()
        {
            _srv = new RestAPICalls();
        }
        public IList<Transactions> GetTransactions()
        {

            try
            {
                return _srv.GetTransactions();
            }
            finally { }
        }
    }
}
