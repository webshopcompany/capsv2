﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Caps.Services
{
    public interface ITransactions
    {
        IList<Transactions> GetTransactions();
    }
}
