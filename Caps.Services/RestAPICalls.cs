﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Caps.Services
{
    public class RestAPICalls
    {
        private readonly string berearToken;
        public RestAPICalls()
        {
            berearToken = $@"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoidmZjX2FwcF91c2VyIn0.4_qBrdKBB5vIk5mT3wQJAwM3P_ZfteOqe3EH5KrUqkA";
        }

        public IList<Transactions> GetTransactions()
        {
            return CallTransactionEP();
        }
        private IList<Transactions> CallTransactionEP()
        {
            IList<Transactions> result;

            var client = new RestClient("http://193.110.250.3:3000/api/transaction?fuel_card=eq.1100015620038&order=transaction_rowno.desc");
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", berearToken);
            IRestResponse response = client.Execute(request);
            Console.WriteLine(response.Content);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                 result = JsonSerializer.Deserialize<IList<Transactions>>(response.Content);
                return result;
            }
            else
            {
                result = new List<Transactions>();
                return result;
            }
        }
    }
}
