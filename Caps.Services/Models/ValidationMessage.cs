﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caps.Services
{
    public class ValidationMessage
    {
        public enum ValidateMessageType
        {
            Error = 0,
            Warning,
            Information
        }
        public ValidateMessageType Type { get; set; }
        public string Message { get; set; }

        public static ValidationMessage Warning(string value, params object[] args)
        {
            return new ValidationMessage { Message = string.Format(value, args), Type = ValidateMessageType.Warning };
        }

        public static ValidationMessage Information(string value, params object[] args)
        {
            return new ValidationMessage { Message = string.Format(value, args), Type = ValidateMessageType.Information };
        }

        public static ValidationMessage Error(string value, params object[] args)
        {
            return new ValidationMessage { Message = string.Format(value, args), Type = ValidateMessageType.Error };
        }

    }
}
