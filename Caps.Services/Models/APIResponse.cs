﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caps.Services
{
    public class APIResponse<T>
    {
        public IList<ValidationMessage> Messages { get; set; }

        public int StatusCode { get; set; }

        public bool Success { get; set; }

        public T ModelData { get; set; }
    }
}
