﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caps.Services
{
    public class Transactions
    {
        public int transaction_rowno { get; set; }
        public string transaction_date { get; set; }
        public string transaction_time { get; set; }
        public string station { get; set; }
        public string article { get; set; }
        public double quantity { get; set; }
        public string sale_unit { get; set; }
        public double sale_price { get; set; }
        public double sale_total_price { get; set; }
        public double official_price { get; set; }
        public double discount { get; set; }
        public int invoice_number { get; set; }
        public string customer_number_billing { get; set; }
        public string customer_number_delivery { get; set; }
        public object fuel_card { get; set; }
        public string plate_number { get; set; }
        public string driver { get; set; }
        public int mileage { get; set; }
        public string customer_grouping_code { get; set; }
        public int recordnr { get; set; }
        public int year { get; set; }
        public int month { get; set; }
    }
}
