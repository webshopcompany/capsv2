﻿using Caps.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapsNew.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private TransactionSrv _srv;
        public TransactionsController()
        {
            _srv = new TransactionSrv();
        }

        [HttpGet("GetTransactions")]
        public IActionResult GetTransactions()
        {
            try
            {
                var list = _srv.GetTransactions();

                return Ok(new APIResponse<IList<Transactions>>() { ModelData = list, StatusCode = 200, Success = true });
            }
            catch (Exception e)
            {
                return BadRequest(new APIResponse<string>() { ModelData = $"Failed - {e.Message}", StatusCode = 201, Success = false });
            }
        }

    }
}
