import { Component, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-fetch-data",
  templateUrl: "./fetch-data.component.html",
})
export class FetchDataComponent {
  public transactions: transaction[];

  constructor(http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    http.get<TransactionListReturn>(baseUrl + "api/transactions/GetTransactions").subscribe(
      (result) => {
        if (result.success) {
          this.transactions = result.modelData;
        }else{
          console.error(result.messages);
        }
      },
      (error) => console.error(error)
    );
  }
}

class transaction {
  transaction_rowno: number;
  transaction_date: string;
  transaction_time: string;
  station: string;
  article: string;
  quantity: number;
  sale_unit: string;
  sale_price: number;
  sale_total_price: number;
  official_price: number;
  discount: number;
  invoice_number: number;
  customer_number_billing: string;
  customer_number_delivery: string;
  fuel_card: any;
  plate_number: string;
  driver: string;
  mileage: number;
  customer_grouping_code: string;
  recordnr: number;
  year: number;
  month: number;
}

class TransactionListReturn {
  messages?: any;
  statusCode: number;
  success: boolean;
  modelData: transaction[];
}
